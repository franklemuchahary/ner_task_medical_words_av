# README #

### INNOPLEXUS NAMED ENTITY RECOGNITION Task on Analytics Vidhya

Named Entity Recognition (NER) is a fundamental Natural Language Processing (NLP) task to extract entities of interest (e.g., disease names, medication names and lab tests) from clinical narratives, thus to support clinical and translational research. Clinical notes have been analyzed in greater detail to harness important information for clinical research and other healthcare operations, as they depict rich, detailed medical information. 

The task is to train a model which can correctly identify medically relevant words present in a given text. 

For example, here is a sentence from a clinical report: 
We compared the inter-day reproducibility of post-occlusive **reactive hyperemia** (PORH) assessed by single- point laser Doppler flowmetry (LDF) and laser speckle contrast analysis (LSCI). 
We need to correctly identify 'reactive hyperemia' from the above text

### APPROACH

Followed the approaches mentioned below to score a **Private LB Score of 79.5 and Rank 17th**

#### CRF using sklearn_crfsuite

* POS Tagging using nltk library
* Tested baseline CRF Model on basic features.
* Feature Extraction/Generation like casings related to the words, next word in the sequence, whether the word in question is a digit, slices of words, pos tags etc.
* Trained CRF Model on the above generated features and tuned parameters using Randomized Search + Manual Tuning. 
* Blending predictions from 4 CRF Models using a weighted average approach. Also tried simple mean blending and meta model stacking. Weighted Average approach performed best.


#### DEEP LEARNING -> BIDIRECTIONAL LSTM and BI-LSTM + CNN

Tested two BILSTM models but was not able to properly implement it due to computational and time constraints. Not able to figure out how to deal with lost information in test due to sequence padding.

* POS Tagging using nltk library
* Baseline Simple BILSTM Model on base created generated using just words. ~ 75.5 Public LB  
* Feature Extraction/Generation Similar to CRF.
* BILSTM Model with multiple inputs corresponding to words, casings, pos_tags etc. ~ 75 Public LB

