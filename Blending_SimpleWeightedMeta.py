
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np


# #### Load Actual Data

# In[60]:

submission = pd.read_csv('../sample_submission.csv')
train = pd.read_csv('../train.csv')


# ####  Load Test Probability Preds

# In[26]:

model1_test = pd.read_csv('CRF_Model1_Test_Probab_Preds.csv')
model2_test = pd.read_csv('CRF_Model2_Test_Probab_Preds.csv')
model4_test = pd.read_csv('CRF_Model4_Test_Probab_Preds.csv')
model6_test = pd.read_csv('CRF_Model6_Test_Probab_Preds.csv')


# In[142]:

model7_test = pd.read_csv('CRF_Model7_Test_Probab_Preds.csv')


# In[234]:

model8_test = pd.read_csv('CRF_Model8_Test_Probab_Preds.csv')


# In[235]:

model8_test.head()


# ### Simple Mean Blend

# In[204]:

blend_test = pd.concat([model1_test, model2_test, model4_test, model6_test, model7_test], 
                        keys=['m1', 'm2', 'm4', 'm6', 'm7'], axis=1)

blend_test.columns = ['_'.join(col).strip() for col in blend_test.columns.values]


# In[205]:

blend_test.columns


# In[30]:

result_blend_test = blend_test.groupby(blend_test.columns, axis=1).mean()


# In[31]:

result_blend_test = result_blend_test.idxmax(axis=1)


# In[32]:

result_blend_test.values


# In[33]:

submission['tag'] = result_blend_test.values


# In[34]:

submission['tag'].value_counts()


# In[35]:

submission.to_csv('blend_test_result_simple_mean_blend_MODELS_1_2_4_6.csv', index=False)


# ### Weighted Blend

# In[241]:

b_indics = ( (0.1*model1_test['B-indications']) + (0.05*model2_test['B-indications']) +              (0.5*model4_test['B-indications']) + (0.05*model6_test['B-indications']) +             (0.3*model7_test['B-indications']) )
 
i_indics = ( (0.1*model1_test['I-indications']) + (0.05*model2_test['I-indications']) +              (0.5*model4_test['I-indications']) + (0.05*model6_test['I-indications']) +              (0.3*model7_test['I-indications']) )

o = ( (0.1*model1_test['O']) + (0.05*model2_test['O']) +             (0.5*model4_test['O']) + (0.05*model6_test['O']) + (0.3*model7_test['O']) )

blend_1 = pd.DataFrame({
    'B-indications': b_indics.values,
    'I-indications': i_indics.values,
    'O': o
})


# In[240]:

b_indics = ( (0.2*model1_test['B-indications']) + (0.1*model2_test['B-indications']) +              (0.6*model4_test['B-indications']) + (0.1*model6_test['B-indications']) )
 
i_indics = ( (0.2*model1_test['I-indications']) + (0.1*model2_test['I-indications']) +              (0.6*model4_test['I-indications']) + (0.1*model6_test['I-indications']) )

o = ( (0.2*model1_test['O']) + (0.1*model2_test['O']) +             (0.6*model4_test['O']) + (0.1*model6_test['O']) )

blend_2 = pd.DataFrame({
    'B-indications': b_indics.values,
    'I-indications': i_indics.values,
    'O': o
})


# In[239]:

b_indics = ( (0.25*blend_1['B-indications']) + (0.65*blend_2['B-indications']) +              (0.1*model4_test['B-indications']) )
 
i_indics = ( (0.25*blend_1['I-indications']) + (0.65*blend_2['I-indications']) +              (0.1*model4_test['I-indications']) )

o = ( (0.25*blend_1['O']) + (0.65*blend_2['O']) +             (0.1*model4_test['O']) )

blend_3 = pd.DataFrame({
    'B-indications': b_indics.values,
    'I-indications': i_indics.values,
    'O': o
})


# In[257]:

b_indics = ( (0.15*model4_test['B-indications']) + (0.2*blend_2['B-indications']) +              (0.5*blend_3['B-indications']) + (0.15*model8_test['B-indications']) )
 
i_indics = ( (0.15*model4_test['I-indications']) + (0.2*blend_2['I-indications']) +              (0.5*blend_3['I-indications']) + (0.15*model8_test['I-indications']) )

o = ( (0.15*model4_test['O']) + (0.2*blend_2['O']) +             (0.5*blend_3['O']) + (0.15*model8_test['O']) )

weighted_blend_df = pd.DataFrame({
    'B-indications': b_indics.values,
    'I-indications': i_indics.values,
    'O': o
})


# In[258]:

weighted_blend_results = weighted_blend_df.idxmax(axis=1).values


# In[259]:

submission['tag'] = weighted_blend_results


# In[260]:

submission['tag'].value_counts()


# In[261]:

submission.to_csv('blend_test_result_weighted_blend_MODELS_m4_b2_b3lowweight_m8.csv', index=False)


# ### Meta Model

# In[51]:

model1_train = pd.read_csv('CRF_Model1_Train_Probab_Preds.csv')
model2_train = pd.read_csv('CRF_Model2_Train_Probab_Preds.csv')
model4_train = pd.read_csv('CRF_Model4_Train_Probab_Preds.csv')
model5_train = pd.read_csv('CRF_Model4_Train_Probab_Preds.csv')
model6_train = pd.read_csv('CRF_Model6_Train_Probab_Preds.csv')


# In[143]:

model7_train = pd.read_csv('CRF_Model7_Train_Probab_Preds.csv')


# In[52]:

model1_train.head()


# In[170]:

concat_train = pd.concat([model1_train, model2_train, model4_train, model6_train, model7_train], 
                         keys=['m1', 'm2', 'm4', 'm6', 'm7'], axis=1).iloc[0:train.shape[0]]


# In[171]:

concat_train.columns = ['_'.join(col).strip() for col in concat_train.columns.values]


# In[172]:

concat_train.head()


# In[173]:

y = train['tag']


# In[197]:

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.metrics import classification_report
import lightgbm as lgb
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression


# #### Meta Model

# #### Load Train Probabilities

# In[189]:

y_tags = y.unique()

def convert_numeric_tags(value):
    mapping = {
        y_tags[0]:0,
        y_tags[1]:1,
        y_tags[2]:2
    }
    
    return mapping[value]

y = y.apply(convert_numeric_tags)


# In[191]:

train_x, test_x, train_y, test_y = train_test_split(concat_train, y, stratify=y, test_size=0.2, random_state=0)


# In[192]:

cv_split =StratifiedKFold(n_splits=3, random_state=0, shuffle=False)


# In[208]:

all_cv_test_preds_probab = []

cv_counter = 1

for train_idx, val_idx in cv_split.split(concat_train, y):
    print(cv_counter, end="\n\n")
    
    t_x = concat_train.iloc[train_idx]
    v_x = concat_train.iloc[val_idx]
    
    t_y = y.iloc[train_idx]
    v_y = y.iloc[val_idx]
    
    model = LogisticRegression(n_jobs=8, random_state=909, C=2, penalty='l2', solver='lbfgs', verbose=10)
    model.fit(t_x, t_y)
    
    val_preds_rf = model.predict(v_x)
    val_preds_rf_probab = model.predict_proba(v_x)
    
    print(classification_report(v_y, val_preds_rf))
    
    print("======"*6, end="\n\n")
    
    test_preds_rf_probab = model.predict_proba(blend_test)
    
    all_cv_test_preds_probab.append(test_preds_rf_probab)
    
    cv_counter+=1


# In[215]:

cv_df_test_preds = pd.DataFrame(all_cv_test_preds_probab[0], columns=model.classes_)

for i in all_cv_test_preds_probab[1:]: 
    cv_df_test_preds = pd.concat([cv_df_test_preds, 
                                  pd.DataFrame(i, columns=model.classes_)], 
                                 axis=1)


# In[216]:

cv_df_test_preds.head()


# In[220]:

col_names_cv = np.concatenate([y_tags for _ in range(3)])


# In[221]:

cv_df_test_preds.columns = col_names_cv


# In[222]:

cv_meta_model_preds = cv_df_test_preds.groupby(cv_df_test_preds.columns, axis=1).mean().idxmax(axis=1).values


# In[223]:

cv_meta_model_preds


# In[224]:

submission['tag'] = cv_meta_model_preds


# In[225]:

submission['tag'].value_counts()


# In[226]:

submission.to_csv('blend_test_result_meta_model_LOGIT_blend_MODELS_1_2_4_6.csv', index=False)

