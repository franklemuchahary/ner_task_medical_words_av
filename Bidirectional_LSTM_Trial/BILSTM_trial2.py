
# coding: utf-8

# In[ ]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
get_ipython().magic('matplotlib inline')
import warnings 

from copy import deepcopy
from collections import namedtuple
from tqdm import tqdm_notebook
import gc
import pickle

import nltk
from nltk.chunk.regexp import RegexpChunkParser, ChunkRule, RegexpParser
from nltk.tree import Tree


# In[ ]:

get_ipython().system(' ls ../input/')
get_ipython().system(' ls')


# In[ ]:

from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.models import Model, Input, load_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score, recall_score, f1_score

from keras.callbacks import ModelCheckpoint, EarlyStopping
import keras.backend as K
import tensorflow as tf
from keras.callbacks import Callback
from keras.optimizers import Adam, Nadam
from keras.layers import LeakyReLU

from keras.models import Model
from keras.layers import TimeDistributed,Conv1D,Dense,Embedding,Input,Dropout,LSTM,Bidirectional,MaxPooling1D,Flatten,concatenate
from keras.utils import Progbar
from keras.preprocessing.sequence import pad_sequences
from keras.initializers import RandomUniform


# ## Scoring Function

# In[ ]:

# Evaluation metric for Innoplexus NER Challenge

def collect_named_entities(tokens): # Helper Function for score calculation
    """
    Creates a list of Entity named-tuples, storing the entity type and the start and end
    offsets of the entity.
    :param tokens: a list of labels
    :return: a list of Entity named-tuples
    """
    Entity = namedtuple("Entity", "e_type start_offset end_offset")
    named_entities = []
    start_offset = None
    end_offset = None
    ent_type = None

    for offset, token_tag in enumerate(tokens):

        if token_tag == 'O':
            if ent_type is not None and start_offset is not None:
                end_offset = offset - 1
                named_entities.append(Entity(ent_type, start_offset, end_offset))
                start_offset = None
                end_offset = None
                ent_type = None

        elif ent_type is None:
            ent_type = token_tag[2:]
            start_offset = offset

        elif ent_type != token_tag[2:] or (ent_type == token_tag[2:] and token_tag[:1] == 'B'):

            end_offset = offset - 1
            named_entities.append(Entity(ent_type, start_offset, end_offset))

            # start of a new entity
            ent_type = token_tag[2:]
            start_offset = offset
            end_offset = None

    # catches an entity that goes up until the last token
    if ent_type and start_offset and end_offset is None:
        named_entities.append(Entity(ent_type, start_offset, len(tokens)-1))

    return named_entities

def compute_metrics(true_named_entities, pred_named_entities): # Helper Function for score calculation
    eval_metrics = {'correct': 0, 'partial': 0, 'missed': 0, 'spurius': 0}
    target_tags_no_schema = ['indications']

    # overall results
    evaluation = {'partial': deepcopy(eval_metrics)}


    true_which_overlapped_with_pred = []  # keep track of entities that overlapped

    # go through each predicted named-entity
    for pred in pred_named_entities:
        found_overlap = False

        # check if there's an exact match, i.e.: boundary and entity type match
        if pred in true_named_entities:
            true_which_overlapped_with_pred.append(pred)
            evaluation['partial']['correct'] += 1

        else:

            # check for overlaps with any of the true entities
            for true in true_named_entities:

                
                # 2. check for an overlap i.e. not exact boundary match, with true entities
                if pred.start_offset <= true.end_offset and true.start_offset <= pred.end_offset:

                    true_which_overlapped_with_pred.append(true)

                    evaluation['partial']['partial'] += 1

                    found_overlap = True
                    break

            # count spurius (i.e., False Positive) entities
            if not found_overlap:
                # overall results
                evaluation['partial']['spurius'] += 1

    # count missed entities (i.e. False Negative)
    for true in true_named_entities:
        if true in true_which_overlapped_with_pred:
            continue
        else:
            # overall results
            evaluation['partial']['missed'] += 1

    # Compute 'possible', 'actual'
    for eval_type in ['partial']:

        correct = evaluation[eval_type]['correct']
        partial = evaluation[eval_type]['partial']
        missed = evaluation[eval_type]['missed']
        spurius = evaluation[eval_type]['spurius']

        # possible: nr. annotations in the gold-standard which contribute to the final score
        evaluation[eval_type]['possible'] = correct + partial + missed

        # actual: number of annotations produced by the NER system
        evaluation[eval_type]['actual'] = correct + partial + spurius

        actual = evaluation[eval_type]['actual']
        possible = evaluation[eval_type]['possible']

    return evaluation

def list_converter(df): # Helper Function for score calculation
    keys, values = df.sort_values('Sent_ID_x').values.T
    ukeys, index = np.unique(keys,True)
    lists = [list(array) for array in np.split(values,index[1:])]
    return lists

# ideal and pred respectively represent dataframes containing actual labels and predictions for the set of sentences in the test data. 
# It has the same format as the sample submission (id, Sent_ID, tag)

def calculate_score(ideal, pred): # Calculates the final F1 Score

    merged = ideal.merge(pred, on = "id", how="inner").drop(['Sent_ID_y'],axis = 1)
    
    
    # The scores are calculated sentence wise and then aggregated to calculate the overall score, for this
    # List converter function groups the labels by sentence to generate a list of lists with each inner list representing a sentence in sequence
    ideal_ = list_converter(merged.drop(['id','tag_y'],axis = 1))
    pred_ = list_converter(merged.drop(['id','tag_x'],axis = 1))

    metrics_results = {'correct': 0, 'partial': 0,
                   'missed': 0, 'spurius': 0, 'possible': 0, 'actual': 0}

    results = {'partial': deepcopy(metrics_results)}


    for true_ents, pred_ents in zip(ideal_, pred_):    
    # compute results for one sentence
        tmp_results = compute_metrics(collect_named_entities(true_ents),collect_named_entities(pred_ents))
    
    # aggregate overall results
        for eval_schema in results.keys():
            for metric in metrics_results.keys():
                results[eval_schema][metric] += tmp_results[eval_schema][metric]
    correct = results['partial']['correct']
    partial = results['partial']['partial']
    missed = results['partial']['missed']
    spurius = results['partial']['spurius']
    actual = results['partial']['actual']
    possible = results['partial']['possible']


    precision = (correct + 0.5 * partial) / actual if actual > 0 else 0
    recall = (correct + 0.5 * partial) / possible if possible > 0 else 0


    score = (2 * precision * recall)/(precision + recall) if (precision + recall) >0 else 0
    
    # final score
    return score


# ## Read Input

# In[ ]:

get_ipython().system(' ls ../input/')


# In[ ]:

train = pd.read_csv('../input/diseasenertask/train.csv')
test = pd.read_csv('../input/diseasenertask/test.csv')
submission = pd.read_csv('../input/diseasenertask/sample_submission.csv')


# In[ ]:

train.head()


# In[ ]:

test.head()


# ### Fill nan values
# Required for adding pos tags

# In[ ]:

train['Word'].fillna(method='bfill', inplace=True)
test['Word'].fillna(method='bfill', inplace=True)


# ## ADD POS Tags to the data

# In[ ]:

pos_tags_train = nltk.pos_tag(train['Word'].values)


# In[ ]:

pos_tags_test = nltk.pos_tag(test['Word'].values)


# In[ ]:

#add pos tags to the main dataset
test['POS_TAG'] = pd.DataFrame(pos_tags_test, columns=['Word', 'POS_TAG'])['POS_TAG'].values
train['POS_TAG'] = pd.DataFrame(pos_tags_train, columns=['Word', 'POS_TAG'])['POS_TAG'].values


# In[ ]:

train.head()


# ## Drop unrequired columns from both train and test

# In[ ]:

dataset = train.drop(['id', 'Doc_ID'], axis=1)
dataset_test = test.drop(['id', 'Doc_ID'], axis=1)


# In[ ]:

dataset.head()


# In[ ]:

dataset_test.head()


# ## Sentence Getter
# Generate tuples containing sentences suitable for Models

# In[ ]:

class SentenceGetter(object):
    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w, p, t) for w, p, t in zip(s['Word'].values.tolist(), 
                                                           s['POS_TAG'].values.tolist(), 
                                                           s['tag'].values.tolist())]
        self.grouped = self.data.groupby('Sent_ID').apply(agg_func)
        self.sentences = [s for s in self.grouped]
        
    def get_next(self):
        try: 
            s = self.grouped['Sentence: {}'.format(self.n_sent)]
            self.n_sent += 1
            return s 
        except:
            return None
        
        
class SentenceGetterTest(object):
    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w, p) for w, p in zip(s['Word'].values.tolist(), 
                                                           s['POS_TAG'].values.tolist())]
        self.grouped = self.data.groupby('Sent_ID').apply(agg_func)
        self.sentences = [s for s in self.grouped]
        
    def get_next(self):
        try: 
            s = self.grouped['Sentence: {}'.format(self.n_sent)]
            self.n_sent += 1
            return s 
        except:
            return None


# In[ ]:

#extract tuples of sentences for train
sentence_getter = SentenceGetter(dataset)


# In[ ]:

#extract sentences for test
sentence_getter_test = SentenceGetterTest(dataset_test)


# In[ ]:

#extract sentences in tuple format
sentences = sentence_getter.sentences
sentences_test = sentence_getter_test.sentences


# In[ ]:

sentences[0]


# In[ ]:

sentences_test[0]


# Max Length of Sentences

# In[ ]:

# distribution of length of sentences
test_sen_lens = [len(s) for s in sentences_test]
print(max(test_sen_lens))

sen_lens = [len(s) for s in sentences]
sns.distplot(sen_lens)
plt.show()


# ## Map Words, Target Tags and POS Tags  to Numbers

# In[ ]:

words_train_test = pd.concat([dataset["Word"], dataset_test['Word']]).apply(lambda x: x.lower())

pos_tags_train_test = pd.concat([dataset["POS_TAG"], dataset_test['POS_TAG']])

words_train_test_last3 = pd.concat([dataset["Word"], dataset_test['Word']]).apply(lambda x: x[-3:])
words_train_test_last2 = pd.concat([dataset["Word"], dataset_test['Word']]).apply(lambda x: x[-2:])

pos_tags_train_test_first2 = pos_tags_train_test.apply(lambda x: x[:2])


# In[ ]:

#list of words
words = list(set(words_train_test.values))
n_words = len(words); n_words


# In[ ]:

#list of last 3 characters of words
words_last3 = list(set(words_train_test_last3))
n_words_last3 = len(words_last3); n_words_last3


# In[ ]:

#list of last 3 characters of words
words_last2 = list(set(words_train_test_last2))
n_words_last2 = len(words_last2); n_words_last2


# In[ ]:

#list of pos tags
pos_tags_list = list(set(pos_tags_train_test))
n_words_pos_tags = len(pos_tags_list); n_words_pos_tags


# In[ ]:

#list of pos tags
pos_tags_list_first2 = list(set(pos_tags_train_test_first2))
n_words_pos_tags2 = len(pos_tags_list_first2); n_words_pos_tags2


# In[ ]:

tags = list(set(dataset["tag"].values))
n_tags = len(tags); n_tags


# ## Generate number mappings for words , target tags and pos_tags

# In[ ]:

#words 
word2idx = {w: i for i, w in enumerate(words)}
word2idx_last3 = {str(w): i for i, w in enumerate(words_last3)}
word2idx_last2 = {str(w): i for i, w in enumerate(words_last2)}

#pos tags
pos2idx = {p: i for i, p in enumerate(pos_tags_list)}
pos2idx_first2 = {p: i for i, p in enumerate(pos_tags_list_first2)}

#target tag
tag2idx = {t: i for i, t in enumerate(tags)}


# In[ ]:

tag2idx


# In[ ]:

#save dictionaries as pickle

dict_pickle_list = [(word2idx, 'word2idx.pickle'), (word2idx_last3, 'word2idx_last3.pickle'), (word2idx_last2, 'word2idx_last2.pickle'), (pos2idx, 'pos2idx.pickle'),
(pos2idx_first2, 'pos2idx_first2.pickle'), (tag2idx, 'tag2idx.pickle')]

ACTION = 'READ'
pickles_list = []

for i in dict_pickle_list:
    if ACTION == 'WRITE':
        pickle_out = open(i[1],"wb+")
        pickle.dump(i[0], pickle_out)
        pickle_out.close()
    
    if ACTION == "READ":
        pickle_in = open(i[1],"rb")
        pickles_list.append(pickle.load(pickle_in))


# In[ ]:

if ACTION == 'READ':
    word2idx = pickles_list[0]
    word2idx_last3 = pickles_list[1]
    word2idx_last2 = pickles_list[2]

    #pos tags
    pos2idx = pickles_list[3]
    pos2idx_first2 = pickles_list[4]

    #target tag
    tag2idx = pickles_list[5]


# In[ ]:

get_ipython().system(' ls')


# ## Feature Extraction

# In[ ]:

#function to extract casings
def getCasings(word):
    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))
    
    case_lookup = {
        'allUpper': 0,
        'allLower': 1,
        'isDigit': 2,
        'digitFractionMoreNumbers': 3,
        'isTitle': 4,
        'containsDigit': 5,
        'other': 6
    }
    
    casing = 'other'
    
    if word.isdigit():  # Is a digit
        casing = 'isDigit'
    elif digitFraction > 0.5:
        casing = 'digitFractionMoreNumbers'
    elif word.islower():  # All lower case
        casing = 'allLower'
    elif word.isupper():  # All upper case
        casing = 'allUpper'
    elif word.istitle():  # is a title, initial char upper, then all lower
        casing = 'isTitle'
    elif numDigits > 0:
        casing = 'containsDigit'

    return case_lookup[casing]


# In[ ]:

#function to generate features
def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    
    features = { 
        'word_lower': word2idx[word.lower()],
        'word_last3': word2idx_last3[word[-3:]],
        'casings': getCasings(word),
        'postag': pos2idx[postag],
        'postag_first2': pos2idx_first2[postag[:2]],
    }
    
    return list(features.values())


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [tag2idx[label] for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]


# In[ ]:

X = [sent2features(s) for s in sentences]
X_Test = [sent2features(s) for s in sentences_test]
y = [sent2labels(s) for s in sentences]


# #### Pad Sequences

# In[ ]:

padding_value = 999333999333

X = pad_sequences(maxlen=140, sequences=X, padding="post", value=padding_value)

X_Test = pad_sequences(maxlen=140, sequences=X_Test, padding="post", value=padding_value)

y = pad_sequences(maxlen=140, sequences=y, padding="post", value=tag2idx["O"])


# In[ ]:

padding_array = X[0][50]
padding_array


# In[ ]:

#convert to categorical
y = [to_categorical(i, num_classes=n_tags) for i in y]


# #### Get Separate Inputs for different layers of Model

# In[ ]:

#word_input_train = [[j[0] for j in i] for i in X]
#word_last3_input_train = [[j[1] for j in i] for i in X]
#casing_input_train = [[j[2] for j in i] for i in X]
#pos_input_train = [[j[3] for j in i] for i in X]
#pos_first2_input_train = [[j[4] for j in i] for i in X]


word_input_test = [[j[0] for j in i] for i in X_Test]
word_last3_input_test = [[j[1] for j in i] for i in X_Test]
casing_input_test = [[j[2] for j in i] for i in X_Test]
pos_input_test = [[j[3] for j in i] for i in X_Test]
pos_first2_input_test = [[j[4] for j in i] for i in X_Test]


# In[ ]:

dataset, dataset_test, sentences, sentences_test = None, None, None, None
words_train_test, words_train_test_last2, words_train_test_last3, pos_tags_train_test, pos_tags_train_test_first2 = None, None, None, None, None
del words_train_test, words_train_test_last2, words_train_test_last3, pos_tags_train_test, pos_tags_train_test_first2
del dataset, dataset_test, sentences, sentences_test
gc.collect()


# In[ ]:

del sentence_getter, sentence_getter_test
gc.collect()

del X, X_Test
gc.collect()
# ## Model Building
# 
# <b> BI-LSTM </b> <br>
# <b> To Try: CNN BI-LSTM </b>

# In[ ]:

def f1_macro_custom(y_true, y_pred):
    y_pred = K.round(y_pred)
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    # tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
    return K.mean(f1)


# In[ ]:

def build_model():
    model = None
    
    words_input = Input(shape=(140,),dtype='int32',name='words_input')
    words = Embedding(input_dim=n_words, output_dim=140, trainable=False)(words_input)
    
    words_input2 = Input(shape=(140,),dtype='int32',name='words_input2')
    words2 = Embedding(input_dim=n_words, output_dim=140, trainable=False)(words_input2)
    
    casing_input = Input(shape=(140,), dtype='int32', name='casing_input')
    casing = Embedding(input_dim=6, output_dim=140, trainable=False)(casing_input)
    
    character_input = Input(shape=(140,),name='char_input')
    embed_char_out = Embedding(n_words_pos_tags, 140, embeddings_initializer=RandomUniform(minval=-0.5, maxval=0.5), trainable=False)(character_input)
    
    character_input2 = Input(shape=(140,),name='char_input2')
    embed_char_out2 = Embedding(n_words_pos_tags, 140, embeddings_initializer=RandomUniform(minval=-0.5, maxval=0.5), trainable=False)(character_input2)
    #conv1d_out= TimeDistributed(Conv1D(kernel_size=3, filters=70, padding='same',activation='tanh', strides=1))(dropout)
    #maxpool_out=TimeDistributed(MaxPooling1D(52))(conv1d_out)
    #char = TimeDistributed(Flatten())(maxpool_out)
    #char = Dropout(0.5)(char)
    
    output = concatenate([words, words2, casing, embed_char_out, embed_char_out2])
    output = Bidirectional(LSTM(200, return_sequences=True, dropout=0.50, recurrent_dropout=0.25))(output)
    output = TimeDistributed(Dense(n_tags, activation='softmax'))(output)
    
    model = Model(inputs=[words_input, words_input2, casing_input, character_input, character_input2], outputs=output)
    
    model.compile(loss='categorical_crossentropy', optimizer='nadam', metrics=["accuracy", f1_macro_custom])
    
    return model

build_model().summary()


# In[ ]:

#define a function to fit the model
def fit_and_evaluate(t_x, t_y, EPOCHS=5, BATCH_SIZE=128, save_model_filename='my_model.h5'):
    pat = 3 #this is the number of epochs with no improvment after which the training will stop
    early_stopping = EarlyStopping(monitor='val_loss', patience=pat, verbose=1)

    #define the model checkpoint callback -> this will keep on saving the model as a physical file
    model_checkpoint = ModelCheckpoint(save_model_filename, verbose=1, save_best_only=True, monitor='val_loss')
    
    model = None
    model = build_model()
    results = model.fit(t_x, t_y, epochs=EPOCHS, batch_size=BATCH_SIZE, callbacks=[early_stopping, model_checkpoint], 
              verbose=1, validation_split=0.1)
    return model, results

returned_model, results = fit_and_evaluate([word_input_train, word_last3_input_train, casing_input_train, pos_input_train, pos_first2_input_train], np.array(y), 
                                           EPOCHS=30, BATCH_SIZE=500)
# In[ ]:

returned_model = load_model('my_model.h5', custom_objects={'f1_macro_custom':f1_macro_custom})

val_preds = returned_model.predict(X_test)
# In[ ]:

test_preds = returned_model.predict([word_input_test, word_last3_input_test, casing_input_test, pos_input_test, pos_first2_input_test])

train_preds = returned_model.predict(X)
# In[ ]:

preds_solid = np.argmax(test_preds, axis=-1)
#preds_solid_train = np.argmax(train_preds, axis=-1)


# In[ ]:

preds_solid


# In[ ]:

endpad_idx = padding_array

test_unique_sent_id = test['Sent_ID'].unique()
#train_unique_sent_id = dataset['Sent_ID'].unique()

test_ids = test['id']
#train_ids = train['id']


# In[ ]:

reverse_tag2idx = {v: k for k, v in tag2idx.items()}


# In[ ]:

reverse_tag2idx


# In[ ]:

def make_preds_df(X_Test, test, test_unique_sent_id, endpad_idx, preds_solid):
    preds_complete = []
    sentence_ids = []
    ids = []

    for i in tqdm_notebook(range(len(X_Test))):
        preds_one = []
        sentence_id_one = []
        id_one = []
    
        temp = test[test['Sent_ID']==test_unique_sent_id[i]]['id'].reset_index(drop=True).values
    
        counter = 0
        for w, pred in zip(X_Test[i][[endpad_idx[0] not in i for i in  X_Test[i]]], preds_solid[i]):
            preds_one.append(reverse_tag2idx[pred])
            sentence_id_one.append(test_unique_sent_id[i])
            id_to_append = temp[counter]
            #print(i)
            #print(counter)
            id_one.append(id_to_append)
            counter+=1
        #break

        preds_complete.extend(preds_one)
        sentence_ids.extend(sentence_id_one)
        ids.extend(id_one)
    
    print(len(sentence_ids))
    print(len(preds_complete))
    print(len(ids))
    
    preds_df = pd.DataFrame({
    'id':ids,
    'tag':preds_complete
    })
    
    return preds_df


# In[ ]:

test_preds_df = make_preds_df(X_Test, test, test_unique_sent_id, endpad_idx, preds_solid)

train_preds_df = make_preds_df(X, train, train_unique_sent_id, endpad_idx, preds_solid_train)
# Merge Proper Submission

# In[ ]:

#test
merged_submission = pd.merge(left=submission[['id', 'Sent_ID']], right=test_preds_df, how='left', on='id')
print(merged_submission.shape)
merged_submission.isnull().sum(axis=0)
merged_submission.fillna(value='O', inplace=True)


# In[ ]:

merged_submission.to_csv('test_results.csv', index=False)
merged_submission.head(10)


# In[ ]:

#train
merged_submission_train = pd.merge(left=train[['id', 'Sent_ID']], right=train_preds_df, how='left', on='id')
print(merged_submission_train.shape)
merged_submission_train.isnull().sum(axis=0)
merged_submission_train.fillna(value='O', inplace=True)


# In[ ]:

merged_submission.to_csv('train_results.csv', index=False)
merged_submission_train.head(10)


# In[ ]:

train[['id', 'Sent_ID', 'tag']].head(10)


# In[ ]:

calculate_score(merged_submission_train, train[['id', 'Sent_ID', 'tag']])


# Download
from IPython.display import Javascript
js_download = """
var csv = '%s';

var filename = 'submission_bilstm_multivariate_3.csv';
var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
if (navigator.msSaveBlob) { // IE 10+
navigator.msSaveBlob(blob, filename);
} else {
var link = document.createElement("a");
if (link.download !== undefined) { // feature detection
// Browsers that support HTML5 download attribute
var url = URL.createObjectURL(blob);
link.setAttribute("href", url);
link.setAttribute("download", filename);
link.style.visibility = 'hidden';
document.body.appendChild(link);
link.click();
document.body.removeChild(link);
}
}
""" % merged_submission.to_csv(index=False).replace('\n','\\n').replace("'","\'")
Javascript(js_download)