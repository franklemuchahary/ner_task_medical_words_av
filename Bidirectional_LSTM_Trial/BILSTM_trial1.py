
# coding: utf-8

# ### Load Libraries

# In[1]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
get_ipython().magic('matplotlib inline')
import warnings 

from copy import deepcopy
from collections import namedtuple
from tqdm import tqdm_notebook
import gc
import pickle


# In[2]:

get_ipython().system(' ls ')


# In[3]:

from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.models import Model, Input, load_model
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Dropout, Bidirectional
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score, recall_score, f1_score

from keras.callbacks import ModelCheckpoint, EarlyStopping
import keras.backend as K
import tensorflow as tf
from keras.callbacks import Callback
from keras.optimizers import Adam
from keras.layers import LeakyReLU


# ### Scoring Metric

# In[4]:

# Evaluation metric for Innoplexus NER Challenge

def collect_named_entities(tokens): # Helper Function for score calculation
    """
    Creates a list of Entity named-tuples, storing the entity type and the start and end
    offsets of the entity.
    :param tokens: a list of labels
    :return: a list of Entity named-tuples
    """
    Entity = namedtuple("Entity", "e_type start_offset end_offset")
    named_entities = []
    start_offset = None
    end_offset = None
    ent_type = None

    for offset, token_tag in enumerate(tokens):

        if token_tag == 'O':
            if ent_type is not None and start_offset is not None:
                end_offset = offset - 1
                named_entities.append(Entity(ent_type, start_offset, end_offset))
                start_offset = None
                end_offset = None
                ent_type = None

        elif ent_type is None:
            ent_type = token_tag[2:]
            start_offset = offset

        elif ent_type != token_tag[2:] or (ent_type == token_tag[2:] and token_tag[:1] == 'B'):

            end_offset = offset - 1
            named_entities.append(Entity(ent_type, start_offset, end_offset))

            # start of a new entity
            ent_type = token_tag[2:]
            start_offset = offset
            end_offset = None

    # catches an entity that goes up until the last token
    if ent_type and start_offset and end_offset is None:
        named_entities.append(Entity(ent_type, start_offset, len(tokens)-1))

    return named_entities

def compute_metrics(true_named_entities, pred_named_entities): # Helper Function for score calculation
    eval_metrics = {'correct': 0, 'partial': 0, 'missed': 0, 'spurius': 0}
    target_tags_no_schema = ['indications']

    # overall results
    evaluation = {'partial': deepcopy(eval_metrics)}


    true_which_overlapped_with_pred = []  # keep track of entities that overlapped

    # go through each predicted named-entity
    for pred in pred_named_entities:
        found_overlap = False

        # check if there's an exact match, i.e.: boundary and entity type match
        if pred in true_named_entities:
            true_which_overlapped_with_pred.append(pred)
            evaluation['partial']['correct'] += 1

        else:

            # check for overlaps with any of the true entities
            for true in true_named_entities:

                
                # 2. check for an overlap i.e. not exact boundary match, with true entities
                if pred.start_offset <= true.end_offset and true.start_offset <= pred.end_offset:

                    true_which_overlapped_with_pred.append(true)

                    evaluation['partial']['partial'] += 1

                    found_overlap = True
                    break

            # count spurius (i.e., False Positive) entities
            if not found_overlap:
                # overall results
                evaluation['partial']['spurius'] += 1

    # count missed entities (i.e. False Negative)
    for true in true_named_entities:
        if true in true_which_overlapped_with_pred:
            continue
        else:
            # overall results
            evaluation['partial']['missed'] += 1

    # Compute 'possible', 'actual'
    for eval_type in ['partial']:

        correct = evaluation[eval_type]['correct']
        partial = evaluation[eval_type]['partial']
        missed = evaluation[eval_type]['missed']
        spurius = evaluation[eval_type]['spurius']

        # possible: nr. annotations in the gold-standard which contribute to the final score
        evaluation[eval_type]['possible'] = correct + partial + missed

        # actual: number of annotations produced by the NER system
        evaluation[eval_type]['actual'] = correct + partial + spurius

        actual = evaluation[eval_type]['actual']
        possible = evaluation[eval_type]['possible']

    return evaluation

def list_converter(df): # Helper Function for score calculation
    keys, values = df.sort_values('Sent_ID_x').values.T
    ukeys, index = np.unique(keys,True)
    lists = [list(array) for array in np.split(values,index[1:])]
    return lists

# ideal and pred respectively represent dataframes containing actual labels and predictions for the set of sentences in the test data. 
# It has the same format as the sample submission (id, Sent_ID, tag)

def calculate_score(ideal, pred): # Calculates the final F1 Score

    merged = ideal.merge(pred, on = "id", how="inner").drop(['Sent_ID_y'],axis = 1)
    
    
    # The scores are calculated sentence wise and then aggregated to calculate the overall score, for this
    # List converter function groups the labels by sentence to generate a list of lists with each inner list representing a sentence in sequence
    ideal_ = list_converter(merged.drop(['id','tag_y'],axis = 1))
    pred_ = list_converter(merged.drop(['id','tag_x'],axis = 1))

    metrics_results = {'correct': 0, 'partial': 0,
                   'missed': 0, 'spurius': 0, 'possible': 0, 'actual': 0}

    results = {'partial': deepcopy(metrics_results)}


    for true_ents, pred_ents in zip(ideal_, pred_):    
    # compute results for one sentence
        tmp_results = compute_metrics(collect_named_entities(true_ents),collect_named_entities(pred_ents))
    
    # aggregate overall results
        for eval_schema in results.keys():
            for metric in metrics_results.keys():
                results[eval_schema][metric] += tmp_results[eval_schema][metric]
    correct = results['partial']['correct']
    partial = results['partial']['partial']
    missed = results['partial']['missed']
    spurius = results['partial']['spurius']
    actual = results['partial']['actual']
    possible = results['partial']['possible']


    precision = (correct + 0.5 * partial) / actual if actual > 0 else 0
    recall = (correct + 0.5 * partial) / possible if possible > 0 else 0


    score = (2 * precision * recall)/(precision + recall) if (precision + recall) >0 else 0
    
    # final score
    return score


# ### Load Data

# In[5]:

get_ipython().system(' ls ../input/')


# In[6]:

train = pd.read_csv('../input/train.csv')
test = pd.read_csv('../input/test.csv')
submission = pd.read_csv('../input/sample_submission.csv')


# In[7]:

train.head()


# In[8]:

test.head()


# ### Subset Data
# Drop unrequired columns from both train and test

# In[9]:

dataset = train.drop(['id', 'Doc_ID'], axis=1)
dataset_test = test.drop(['id', 'Doc_ID'], axis=1)


# In[10]:

dataset.head()


# In[11]:

dataset_test.head()


# ### Sentence Getter
# Generate tuples containing sentences suitable for Models

# In[12]:

class SentenceGetter(object):
    
    def __init__(self, dataset):
        self.n_sent = 1
        self.dataset = dataset
        self.empty = False
        agg_func = lambda s: [(w, t) for w,t in zip(s["Word"].values.tolist(),
                                                        s["tag"].values.tolist())]
        self.grouped = self.dataset.groupby("Sent_ID").apply(agg_func)
        self.sentences = [s for s in self.grouped]
    
    def get_next(self):
        try:
            s = self.grouped["Sentence: {}".format(self.n_sent)]
            self.n_sent += 1
            return s
        except:
            return None
        
        
class SentenceGetterTest(object):
    
    def __init__(self, dataset):
        self.n_sent = 1
        self.dataset = dataset
        self.empty = False
        agg_func = lambda s: [(w) for w in s["Word"].values.tolist()]
        self.grouped = self.dataset.groupby("Sent_ID").apply(agg_func)
        self.sentences = [s for s in self.grouped]
    
    def get_next(self):
        try:
            s = self.grouped["Sentence: {}".format(self.n_sent)]
            self.n_sent += 1
            return s
        except:
            return None


# In[13]:

#extract tuples of sentences for train
sentence_getter = SentenceGetter(dataset)


# In[14]:

#extract sentences for test
sentence_getter_test = SentenceGetterTest(dataset_test)


# In[15]:

#extract sentences in tuple format
sentences = sentence_getter.sentences
sentences_test = sentence_getter_test.sentences


# In[16]:

sentences[0]


# In[17]:

sentences_test[0]


# In[18]:

test_sen_lens = [len(s) for s in sentences_test]
max(test_sen_lens)


# In[19]:

# distribution of length of sentences
sen_lens = [len(s) for s in sentences]
sns.distplot(sen_lens)
plt.show()


# ### Map Words and Tags to Numbers

# In[20]:

max(sen_lens)


# In[21]:

words = list(set(pd.concat([dataset["Word"], dataset_test['Word']]).values))
words.append("ENDPAD")

n_words = len(words); n_words


# In[22]:

tags = list(set(dataset["tag"].values))

n_tags = len(tags); n_tags


# Generate number mappings for words and tags

# In[23]:

word2idx = {w: i for i, w in enumerate(words)}
tag2idx = {t: i for i, t in enumerate(tags)}
tag2idx

pickle_in = open("tag2idx.pickle","rb")
tag2idx = pickle.load(pickle_in)
tag2idxpickle_in = open("word2idx.pickle","rb")
word2idx = pickle.load(pickle_in)
word2idx
# ### Save as pickle

# In[24]:

pickle_out = open("word2idx.pickle","wb+")
pickle.dump(word2idx, pickle_out)
pickle_out.close()


# In[25]:

pickle_out = open("tag2idx.pickle","wb+")
pickle.dump(tag2idx, pickle_out)
pickle_out.close()


# ### Converting to Model Appropriate Data

# In[26]:

X = [[word2idx[w[0]] for w in s] for s in sentences]


# In[27]:

X_Test = [[word2idx[w] for w in s] for s in sentences_test]


# In[28]:

y = [[tag2idx[w[1]] for w in s] for s in sentences]


# ### Pad Sequences

# In[29]:

X = pad_sequences(maxlen=140, sequences=X, padding="post",value=n_words - 1)
X_Test = pad_sequences(maxlen=140, sequences=X_Test, padding="post",value=n_words - 1)
y = pad_sequences(maxlen=140, sequences=y, padding="post", value=tag2idx["O"])


# In[30]:

#convert to categorical
y = [to_categorical(i, num_classes=n_tags) for i in y]


# ### Model Building

# In[31]:

def build_model():
    model = None
    
    input = Input(shape=(140,))
    model = Embedding(input_dim=n_words, output_dim=140, input_length=140)(input)
    model = Dropout(0.2)(model)
    model = Bidirectional(LSTM(units=100, return_sequences=True, recurrent_dropout=0.1))(model)
    out = TimeDistributed(Dense(n_tags, activation="softmax"))(model) 
    model = Model(input, out)
    
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
    
    return model


# In[32]:

build_model().summary()


# In[33]:

#define a function to fit the model
def fit_and_evaluate(t_x, t_y, EPOCHS=5, BATCH_SIZE=128, save_model_filename='my_model.h5'):
    pat = 3 #this is the number of epochs with no improvment after which the training will stop
    early_stopping = EarlyStopping(monitor='val_loss', patience=pat, verbose=1)

    #define the model checkpoint callback -> this will keep on saving the model as a physical file
    model_checkpoint = ModelCheckpoint(save_model_filename, verbose=1, save_best_only=True, monitor='val_loss')
    
    model = None
    model = build_model()
    results = model.fit(t_x, t_y, epochs=EPOCHS, batch_size=BATCH_SIZE, callbacks=[early_stopping, model_checkpoint], 
              verbose=1, validation_split=0.1)
    return model, results


# In[34]:

returned_model, results = fit_and_evaluate(X, np.array(y), EPOCHS=30, BATCH_SIZE=128)


# In[35]:

returned_model = load_model('my_model.h5')

val_preds = returned_model.predict(X_test)
# In[36]:

test_preds = returned_model.predict(X_Test)


# In[37]:

train_preds = returned_model.predict(X)


# In[38]:

preds_solid = np.argmax(test_preds, axis=-1)
preds_solid_train = np.argmax(train_preds, axis=-1)


# In[39]:

endpad_idx = word2idx['ENDPAD']

test_unique_sent_id = dataset_test['Sent_ID'].unique()
train_unique_sent_id = dataset['Sent_ID'].unique()

test_ids = test['id']
train_ids = train['id']


# In[40]:

def make_preds_df(X_Test, test, test_unique_sent_id, endpad_idx, preds_solid):
    preds_complete = []
    words_complete = []
    sentence_ids = []
    ids = []

    for i in tqdm_notebook(range(len(X_Test))):
        preds_one = []
        words_one = []
        sentence_id_one = []
        id_one = []
    
        temp = test[test['Sent_ID']==test_unique_sent_id[i]]['id'].reset_index(drop=True).values
    
        counter = 0
        for w, pred in zip(X_Test[i][X_Test[i]!=endpad_idx], preds_solid[i]):
            preds_one.append(tags[pred])
            words_one.append(words[w])
            sentence_id_one.append(test_unique_sent_id[i])
            id_to_append = temp[counter]
            #print(i)
            #print(counter)
            id_one.append(id_to_append)
            counter+=1
        #break
        
        words_complete.extend(words_one)
        preds_complete.extend(preds_one)
        sentence_ids.extend(sentence_id_one)
        ids.extend(id_one)
    
    print(len(sentence_ids))
    print(len(words_complete))
    print(len(preds_complete))
    print(len(ids))
    
    preds_df = pd.DataFrame({
    'id':ids,
    'tag':preds_complete
    })
    
    return preds_df


# In[41]:

test_preds_df = make_preds_df(X_Test, test, test_unique_sent_id, endpad_idx, preds_solid)


# In[42]:

train_preds_df = make_preds_df(X, train, train_unique_sent_id, endpad_idx, preds_solid_train)


# Merge Proper Submission

# In[43]:

#test
merged_submission = pd.merge(left=submission[['id', 'Sent_ID']], right=test_preds_df, how='left', on='id')
print(merged_submission.shape)
merged_submission.isnull().sum(axis=0)
merged_submission.fillna(value='O', inplace=True)


# In[44]:

merged_submission.to_csv('test_results.csv', index=False)
merged_submission.head(10)


# In[45]:

#train
merged_submission_train = pd.merge(left=train[['id', 'Sent_ID']], right=train_preds_df, how='left', on='id')
print(merged_submission_train.shape)
merged_submission_train.isnull().sum(axis=0)
merged_submission_train.fillna(value='O', inplace=True)


# In[46]:

merged_submission.to_csv('train_results.csv', index=False)
merged_submission_train.head(10)


# In[47]:

train[['id', 'Sent_ID', 'tag']].head(10)


# In[48]:

calculate_score(merged_submission_train, train[['id', 'Sent_ID', 'tag']])


# Download
from IPython.display import Javascript
js_download = """
var csv = '%s';

var filename = 'submission_bilstm_basic_2.csv';
var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
if (navigator.msSaveBlob) { // IE 10+
navigator.msSaveBlob(blob, filename);
} else {
var link = document.createElement("a");
if (link.download !== undefined) { // feature detection
// Browsers that support HTML5 download attribute
var url = URL.createObjectURL(blob);
link.setAttribute("href", url);
link.setAttribute("download", filename);
link.style.visibility = 'hidden';
document.body.appendChild(link);
link.click();
document.body.removeChild(link);
}
}
""" % merged_submission.to_csv(index=False).replace('\n','\\n').replace("'","\'")
Javascript(js_download)