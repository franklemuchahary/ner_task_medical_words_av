
# coding: utf-8

# ### Load Libraries

# In[90]:

import pandas as pd
import numpy as np
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import Perceptron
from sklearn.model_selection import train_test_split, cross_val_predict
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report

import sklearn_crfsuite
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics
from collections import Counter

import nltk
import tqdm

from copy import deepcopy
from collections import namedtuple


# ### Scoring Function

# In[4]:

# Evaluation metric for Innoplexus NER Challenge

def collect_named_entities(tokens): # Helper Function for score calculation
    """
    Creates a list of Entity named-tuples, storing the entity type and the start and end
    offsets of the entity.
    :param tokens: a list of labels
    :return: a list of Entity named-tuples
    """
    Entity = namedtuple("Entity", "e_type start_offset end_offset")
    named_entities = []
    start_offset = None
    end_offset = None
    ent_type = None

    for offset, token_tag in enumerate(tokens):

        if token_tag == 'O':
            if ent_type is not None and start_offset is not None:
                end_offset = offset - 1
                named_entities.append(Entity(ent_type, start_offset, end_offset))
                start_offset = None
                end_offset = None
                ent_type = None

        elif ent_type is None:
            ent_type = token_tag[2:]
            start_offset = offset

        elif ent_type != token_tag[2:] or (ent_type == token_tag[2:] and token_tag[:1] == 'B'):

            end_offset = offset - 1
            named_entities.append(Entity(ent_type, start_offset, end_offset))

            # start of a new entity
            ent_type = token_tag[2:]
            start_offset = offset
            end_offset = None

    # catches an entity that goes up until the last token
    if ent_type and start_offset and end_offset is None:
        named_entities.append(Entity(ent_type, start_offset, len(tokens)-1))

    return named_entities

def compute_metrics(true_named_entities, pred_named_entities): # Helper Function for score calculation
    eval_metrics = {'correct': 0, 'partial': 0, 'missed': 0, 'spurius': 0}
    target_tags_no_schema = ['indications']

    # overall results
    evaluation = {'partial': deepcopy(eval_metrics)}


    true_which_overlapped_with_pred = []  # keep track of entities that overlapped

    # go through each predicted named-entity
    for pred in pred_named_entities:
        found_overlap = False

        # check if there's an exact match, i.e.: boundary and entity type match
        if pred in true_named_entities:
            true_which_overlapped_with_pred.append(pred)
            evaluation['partial']['correct'] += 1

        else:

            # check for overlaps with any of the true entities
            for true in true_named_entities:

                
                # 2. check for an overlap i.e. not exact boundary match, with true entities
                if pred.start_offset <= true.end_offset and true.start_offset <= pred.end_offset:

                    true_which_overlapped_with_pred.append(true)

                    evaluation['partial']['partial'] += 1

                    found_overlap = True
                    break

            # count spurius (i.e., False Positive) entities
            if not found_overlap:
                # overall results
                evaluation['partial']['spurius'] += 1

    # count missed entities (i.e. False Negative)
    for true in true_named_entities:
        if true in true_which_overlapped_with_pred:
            continue
        else:
            # overall results
            evaluation['partial']['missed'] += 1

    # Compute 'possible', 'actual'
    for eval_type in ['partial']:

        correct = evaluation[eval_type]['correct']
        partial = evaluation[eval_type]['partial']
        missed = evaluation[eval_type]['missed']
        spurius = evaluation[eval_type]['spurius']

        # possible: nr. annotations in the gold-standard which contribute to the final score
        evaluation[eval_type]['possible'] = correct + partial + missed

        # actual: number of annotations produced by the NER system
        evaluation[eval_type]['actual'] = correct + partial + spurius

        actual = evaluation[eval_type]['actual']
        possible = evaluation[eval_type]['possible']

    return evaluation

def list_converter(df): # Helper Function for score calculation
    keys, values = df.sort_values('Sent_ID_x').values.T
    ukeys, index = np.unique(keys,True)
    lists = [list(array) for array in np.split(values,index[1:])]
    return lists

# ideal and pred respectively represent dataframes containing actual labels and predictions for the set of sentences in the test data. 
# It has the same format as the sample submission (id, Sent_ID, tag)

def calculate_score(ideal, pred): # Calculates the final F1 Score

    merged = ideal.merge(pred, on = "id", how="inner").drop(['Sent_ID_y'],axis = 1)
    
    
    # The scores are calculated sentence wise and then aggregated to calculate the overall score, for this
    # List converter function groups the labels by sentence to generate a list of lists with each inner list representing a sentence in sequence
    ideal_ = list_converter(merged.drop(['id','tag_y'],axis = 1))
    pred_ = list_converter(merged.drop(['id','tag_x'],axis = 1))

    metrics_results = {'correct': 0, 'partial': 0,
                   'missed': 0, 'spurius': 0, 'possible': 0, 'actual': 0}

    results = {'partial': deepcopy(metrics_results)}


    for true_ents, pred_ents in zip(ideal_, pred_):    
    # compute results for one sentence
        tmp_results = compute_metrics(collect_named_entities(true_ents),collect_named_entities(pred_ents))
    
    # aggregate overall results
        for eval_schema in results.keys():
            for metric in metrics_results.keys():
                results[eval_schema][metric] += tmp_results[eval_schema][metric]
    correct = results['partial']['correct']
    partial = results['partial']['partial']
    missed = results['partial']['missed']
    spurius = results['partial']['spurius']
    actual = results['partial']['actual']
    possible = results['partial']['possible']


    precision = (correct + 0.5 * partial) / actual if actual > 0 else 0
    recall = (correct + 0.5 * partial) / possible if possible > 0 else 0


    score = (2 * precision * recall)/(precision + recall) if (precision + recall) >0 else 0
    
    # final score
    return score


# In[5]:

import pickle

def save_model(model_obj, filename):
    try:
        with open(filename, 'wb') as fid:
            pickle.dump(model_obj, fid) 
        print("Done")
    except Exception as e: print(e)


def load_saved_pickle_model(filename):
    return pickle.load(open(filename, 'rb'))


# ### Load Data

# In[6]:

train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')
submissions = pd.read_csv('sample_submission.csv')


# ### Preprocessing

# In[7]:

train.head()


# In[8]:

test.head()


# In[9]:

train['Word'].fillna(method='bfill', inplace=True)
test['Word'].fillna(method='bfill', inplace=True)


# POS Tagger

# In[10]:

pos_tags_train = nltk.pos_tag(train['Word'].values)


# In[11]:

pos_tags_test = nltk.pos_tag(test['Word'].values)


# In[12]:

#add pos tags to the main dataset
test['POS_TAG'] = pd.DataFrame(pos_tags_test, columns=['Word', 'POS_TAG'])['POS_TAG'].values
train['POS_TAG'] = pd.DataFrame(pos_tags_train, columns=['Word', 'POS_TAG'])['POS_TAG'].values


# In[13]:

train.head()


# ### Datasets

# In[14]:

dataset = train.drop(['id', 'Doc_ID'], axis=1)
dataset_test = test.drop(['id', 'Doc_ID'], axis=1)


# ### Sentence Getter

# In[15]:

class SentenceGetter(object):
    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w, p, t) for w, p, t in zip(s['Word'].values.tolist(), 
                                                           s['POS_TAG'].values.tolist(), 
                                                           s['tag'].values.tolist())]
        self.grouped = self.data.groupby('Sent_ID').apply(agg_func)
        self.sentences = [s for s in self.grouped]
        
    def get_next(self):
        try: 
            s = self.grouped['Sentence: {}'.format(self.n_sent)]
            self.n_sent += 1
            return s 
        except:
            return None
        
        
class SentenceGetterTest(object):
    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w, p) for w, p in zip(s['Word'].values.tolist(), 
                                                           s['POS_TAG'].values.tolist())]
        self.grouped = self.data.groupby('Sent_ID').apply(agg_func)
        self.sentences = [s for s in self.grouped]
        
    def get_next(self):
        try: 
            s = self.grouped['Sentence: {}'.format(self.n_sent)]
            self.n_sent += 1
            return s 
        except:
            return None


# In[16]:

getter = SentenceGetter(dataset)
getter_test = SentenceGetterTest(dataset_test)


sentences = getter.sentences
sentences_test = getter_test.sentences


# ### Feature Extractions

# In[73]:

def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
       
    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))
    
    features = {
        'bias': 1.0, 
        'word.lower()': word.lower(), 
        'word[-3:]': word[-3:],
        'word[-2:]': word[-2:],
        'word[:3]': word[:3],
        'digit_fraction': digitFraction>0.5,
        'contains_digit': numDigits>0,
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag': postag,
        'postag[:2]': postag[:2],
    }
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        
        numDigits1 = 0
        for char in word1:
            if char.isdigit():
                numDigits1 += 1

        digitFraction1 = numDigits1 / float(len(word1))
        
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:word[-3:]': word1[-3:],
            '-1:digit_fraction': digitFraction1>0.5,
            '-1:contains_digit': numDigits1>0,
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True
    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        
        numDigits1 = 0
        for char in word1:
            if char.isdigit():
                numDigits1 += 1

        digitFraction1 = numDigits1 / float(len(word1))
        
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:word[-3:]': word1[-3:],
            '+1:digit_fraction': digitFraction1>0.5,
            '+1:contains_digit': numDigits1>0,
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True
    
    return features

def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]


# In[74]:

X = [sent2features(s) for s in sentences]
y = [sent2labels(s) for s in sentences]


# In[75]:

X_Test = [sent2features(s) for s in sentences_test]


# ### Conditional Random Field

# In[78]:

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# In[77]:

def manual_oversample(X, y, oversample_counts=1):
    temp_y = np.asarray(y)
    temp_X = np.asarray(X)

    indexes_b = ['B-indications' in i for i in temp_y]
    temp_X_add = temp_X[indexes_b]
    temp_y_add = temp_y[indexes_b]
        
    for i in range(1, oversample_counts+1):
        temp_X = np.concatenate([temp_X, temp_X_add])
        temp_y = np.concatenate([temp_y, temp_y_add])
        
    return list(temp_X), list(temp_y)

X, y = manual_oversample(X, y, 2)
# Train Model

# In[79]:

crf = sklearn_crfsuite.CRF(
    algorithm='lbfgs',
    c1=0.01, #0.01
    c2=0.01, #0.01
    max_iterations=1500,
    all_possible_transitions=True,
    verbose=True
)

crf_model = crf.fit(X_train, y_train)


# In[80]:

save_model(crf_model, 'crf_model8_more_features.pk')


# #### Load Model

# In[81]:

crf_model = load_saved_pickle_model('crf_model8_more_features.pk')


# In[82]:

y_pred = crf_model.predict(X_test)

print(metrics.flat_classification_report(y_test, y_pred))

print(metrics.flat_f1_score(y_test, y_pred, average='weighted'))


# ### Make Preds for Actual Test and Train

# Test

# In[83]:

#solid string predictions for test
y_pred_test = crf_model.predict(X_Test)


# In[84]:

#flatten
flat_y_pred_test = []

for i in tqdm.tqdm_notebook(y_pred_test):
    flat_y_pred_test.extend(i)


# In[88]:

#probability predictions for test
y_pred_test_probab = crf_model.predict_marginals(X_Test)

y_pred_test_probab_flat = []

for i in y_pred_test_probab:
    y_pred_test_probab_flat.extend(i)
    
y_pred_test_probab_flat = pd.DataFrame(y_pred_test_probab_flat)
    
y_pred_test_probab_flat.to_csv('Blending/CRF_Model8_Test_Probab_Preds.csv', index=False)


# Train

# In[67]:

#solid string predictions for train
y_pred_complete_train = crf_model.predict(X)


# In[68]:

#flatten
flat_y_pred_train = []

for i in tqdm.tqdm_notebook(y_pred_complete_train):
    flat_y_pred_train.extend(i)


# In[89]:

#probability predictions for train
y_pred_train_probab = crf_model.predict_marginals(X)

y_pred_train_probab_flat = []

for i in y_pred_train_probab:
    y_pred_train_probab_flat.extend(i)
    
y_pred_train_probab_flat = pd.DataFrame(y_pred_train_probab_flat)
    
y_pred_train_probab_flat.to_csv('Blending/CRF_Model8_Train_Probab_Preds.csv', index=False)


# ### Calculate IOB based score on train

# In[70]:

train_preds_df = train[['id', 'Sent_ID', 'tag']].copy()
train_preds_df['tag'] = flat_y_pred_train


# In[71]:

calculate_score(train_preds_df, train[['id', 'Sent_ID', 'tag']])


# ### Prepare Submissions

# In[85]:

submissions['tag'] = flat_y_pred_test


# In[86]:

submissions['tag'].value_counts()


# In[87]:

submissions.to_csv('submissions history/crf_sklearn_submission_8_lbfgs_more_features.csv', index=False)

